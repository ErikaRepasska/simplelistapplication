package com.example.erika.paraglidingmap.network;

import com.example.erika.paraglidingmap.models.ProxyLaunch;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public class ApiServices {

  private static ServerResponse serverResponse;

  public interface ServerResponse {
    @GET("?action=0")
    Call<List<ProxyLaunch>> getLunchServerResponseModel();

    /*@GET("landing")
    Call<ServerResponseModel<Launch>> getLunchServerResponseModel();

    @GET("parking")
    Call<ServerResponseModel<Launch>> getLunchServerResponseModel();*/
  }

  public static synchronized ServerResponse getServerResponseService() {
    if (serverResponse == null) {
        serverResponse = ApiClients
            .getApiClient()
            .create(ServerResponse.class);
    }

    return serverResponse;
  }
}
