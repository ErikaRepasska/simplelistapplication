package com.example.erika.paraglidingmap.network;

import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class ApiClients {

  private static Retrofit mClient;

  public static synchronized Retrofit getApiClient() {
    if (mClient == null) {
        mClient = new Retrofit
                .Builder()
                .baseUrl("http://api.paraglidingmap.eu/api/v1/endpoint.php/")
                .addConverterFactory
                        (MoshiConverterFactory.create())
                .build();
    }

    return mClient;
  }
}
