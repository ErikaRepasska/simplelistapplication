package com.example.erika.paraglidingmap.network;

import com.example.erika.paraglidingmap.models.ProxyLaunch;

import java.util.List;

import retrofit2.Call;

public class ApiCalls {

  public static Call<List<ProxyLaunch>> getLunchServerResponseCall() {
    return ApiServices
        .getServerResponseService()
        .getLunchServerResponseModel();
  }
}
