package com.example.erika.paraglidingmap;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.erika.paraglidingmap.models.ProxyLaunch;
import com.example.erika.paraglidingmap.network.ApiCalls;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final static int SUCCESS = 0;

    ListView listView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //new DownloadWebPageTask().execute("http://www.paragliding-mapa.cz/api/v0.1/launch");
        loadDataFromServer();
    }

    private void loadDataFromServer() {
        ApiCalls
                .getLunchServerResponseCall()
                .enqueue(new Callback<List<ProxyLaunch>>() {
                    @Override
                    public void onResponse(final @NonNull Call<List<ProxyLaunch>> call,
                                           final @NonNull Response<List<ProxyLaunch>> response) {
                        final List<ProxyLaunch> proxyLaunchList = response.body();
                        if (response.isSuccessful() && proxyLaunchList != null) {
                            fillListView(proxyLaunchList);
                        } else {
                            // ToDO: onResponseUnsuccessful();
                        }
                    }

                    @Override
                    public void onFailure(final @NonNull Call<List<ProxyLaunch>> call, final Throwable t) {
                        Log.d("Erika", "onFailure() called with: call = [" + call + "], t = [" + t + "]");
                        // ToDO: onCallFailure();
                    }
                });
    }

    private void fillListView(final @NonNull List<ProxyLaunch> launches) {
        final ArrayList<String> myList = new ArrayList<>();
        for (final ProxyLaunch proxyLaunch : launches) {
            myList.add("ID: " + proxyLaunch.getId() + "\n" + "Latitude: " + proxyLaunch.getLatitude() + "\n" + "Longitude: "
                    + proxyLaunch.getLongitude() + "\n");
        }
        listView.setAdapter(new ArrayAdapter<>(MainActivity.this,
                android.R.layout.simple_list_item_1, myList));
    }

    @Override
    public void onBackPressed() {
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final @NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
